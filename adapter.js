// Set globals
/* global database pronghornProps log eventSystem */
/* eslint default-param-last: warn */

const path = require('path');
const fs = require('fs');
const { unzip } = require('zlib');
const EventEmitterCl = require('events').EventEmitter;
const util = require('util');

const encProps = pronghornProps.pathProps.encrypted;
const Ajv = require('ajv');

const ajv = new Ajv({ allErrors: true });
const mockDeviceSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, './mockDeviceSchema.json'), 'utf-8')
);
const configMgrPath = path.join(__dirname, '../../@itential/app-configuration_manager');
const utilsPath = path.join(__dirname, '../../@itential/itential-utils');
let MOCK_DEVICES;
let CONFIG_PARSERS;
let getDriver;
let translateChangesToNativeConfig;
let Discovery;
let discovery;

if (process.env.NODE_ENV !== 'test') {
  ({ Discovery } = require(`${utilsPath}`));
  discovery = new Discovery();
  console.log(discovery);
  MOCK_DEVICES = database.collection('mock_devices');
  CONFIG_PARSERS = database.collection('ucm_config_parsers');
  ({ getDriver } = discovery.require(
    `${configMgrPath}/shared/device-config/index.js`,
    encProps
  ));
  ({ translateChangesToNativeConfig } = discovery.require(
    `${configMgrPath}/config-tools/nativeConfigConverter.js`,
    encProps
  ));
}

const getDeviceParser = async (deviceType) => {
  try {
    const parserData = await CONFIG_PARSERS.findOne({ name: deviceType });
    if (parserData) {
      return parserData;
    }
    return null;
  } catch (error) {
    log.error(`Failed to get device parser. ${error.message || error}`);
    return null;
  }
};

// Converts lines of config into changesets
// You need changesets to rebuild a config
/**
 * @param lineObj
 * @param p
 * @param changes
 */
function lineToChangeSet(lineObj, p, changes) {
  const line = lineObj.words.join(' ');
  const parent = [...p];
  if (lineObj.lines.length === 0) {
    changes.push({
      parents: p,
      new: line,
      old: null
    });
  } else {
    parent.push(line);
    lineObj.lines.forEach((element) => {
      lineToChangeSet(element, parent, changes);
    });
  }
}

// Puts ! comments on new lines because they get removed
// This is sketchy at best
/**
 * @param native
 */
function bangOn(native) {
  let indent = 0;
  const newLines = [];
  const lines = native.split('\n');
  lines.forEach((line) => {
    const spacecount = line.search(/\S|$/);
    if (spacecount < indent) {
      let bString = '';
      for (let index = 0; index < spacecount; index += 1) {
        bString += ' ';
      }
      bString += '!';
      newLines.push(bString);
    }
    newLines.push(line);
    indent = spacecount;
  });
  return newLines.join('\n');
}

// Finds misplaced end and moves it to the end
// This is mostly important for cisco-ios
/**
 * @param native
 */
function findEnd(native) {
  const lines = native.split('\n');
  const index = lines.indexOf('end');
  if (index > -1) {
    lines.splice(index, 1);
    lines.push('end');
  }
  return lines.join('\n');
}

/**
 * @param native
 * @param line
 */
function removeLine(native, line) {
  const lines = native.split('\n');
  const index = lines.indexOf(line);
  if (index > -1) {
    lines.splice(index, 1);
  }
  return lines.join('\n');
}

/**
 * @param native
 */
function negation(native) {
  const regex = /no .*/g;
  const lines = native.split('\n');
  const negatives = lines.filter((line) => line.match(regex));

  let updated = native;
  negatives.forEach((n) => {
    const pieces = n.split('no ');
    let ltr = pieces[1];
    if (pieces[0].length > 0) {
      ltr = pieces[0].split('no ') + ltr;
    }
    updated = removeLine(updated, ltr);
    updated = removeLine(updated, n);
  });
  return updated;
}

/**
 * @param data
 */
function decompressData(data) {
  return new Promise((resolve, reject) => {
    const buffer = Buffer.from(data, 'base64');
    unzip(buffer, (error, bufferString) => {
      if (error) reject(error);
      resolve(bufferString.toString());
    });
  });
}

/**
 * @param device
 */
function validateSchema(device) {
  const validate = ajv.compile(mockDeviceSchema);
  const valid = validate(device);
  return valid;
}

class MockDevice {
  constructor(prongid, properties) {
    log.debug(`Mock device properties ${JSON.stringify(properties)}`);
    console.log(`config path: ${configMgrPath}`);
    this.props = properties;
    this.id = prongid;
  }

  connect() {
    setTimeout(() => {
      this.emit('ONLINE', { id: this.id });
      const msg = {
        id: this.id,
        server: 'foo',
        type: 'MockDevice',
        time: Date.now().toString()
      };
      eventSystem.publish('adapterOnline', msg);
    }, 1000);
  }

  healthCheck(callback) {
    const status = {
      id: this.id,
      status: 'success'
    };
    callback(status);
  }

  hasEntities(entityType, deviceList, callback) {
    (async () => {
      if (entityType !== 'Device') {
        return callback(
          null,
          `${this.id} does not support entity ${entityType}`
        );
      }
      // TODO: $in deviceList
      const devices = await MOCK_DEVICES.find({ deleted: false }).toArray();
      const findings = deviceList.reduce((map, device) => {
        map[device] = false;
        devices.forEach((foundDevice) => {
          if (foundDevice.name === device) {
            map[device] = true;
          }
        });
        return map;
      }, {});
      return callback(findings);
    })();
  }

  getDevice(deviceId, callback) {
    (async () => {
      const device = await MOCK_DEVICES.findOne({
        deleted: false,
        name: deviceId
      });
      if (!device) {
        return callback(null, 'Device not found');
      }
      const copy = JSON.parse(JSON.stringify(device));
      delete copy.states;
      delete copy.actions;
      delete copy._id;
      delete copy.deleted;
      delete copy.online;
      return callback(copy);
    })();
  }

  getDevicesFiltered(searchOptions, callback) {
    (async () => {
      const result = await MOCK_DEVICES.find({
        deleted: false,
        name: { $regex: `.*${searchOptions.filter.name}.*` }
      }).toArray();
      const result2 = [];
      // eslint-disable-next-line guard-for-in
      for (const r in result) {
        const validateDevice = result[r];
        if (validateSchema(validateDevice)) {
          delete result[r]._id;
          delete result[r].deleted;
          delete result[r].online;
          result2.push(result[r]);
        }
      }
      // eslint-disable-next-line prefer-object-spread
      Object.assign({}, result2);
      return callback({
        total: result2.length,
        list: result2
      });
    })();
  }

  addDevice(device, callback) {
    return callback({
      code: 0,
      value: reply
    });
  }

  deleteDevice(deviceId, callback) {
    return callback({
      code: 0,
      value: reply
    });
  }

  isAlive(deviceId, callback) {
    (async () => {
      const device = await MOCK_DEVICES.findOne({
        deleted: false,
        name: deviceId
      });
      if (!device) {
        return callback(null, 'Device not found');
      }
      const newDev = {};
      newDev.response = device.online;
      return callback(device.online);
    })();
  }

  getConfig(deviceId, format, callback) {
    (async () => {
      const result = await MOCK_DEVICES.findOne({
        deleted: false,
        name: deviceId
      });
      if (result) {
        if (
          result.config !== undefined
          && Object.hasOwnProperty.call(result.config, 'type')
          && result.config.type === 'Buffer'
        ) {
          const decompressConfig = await decompressData(result.config);

          return callback({
            code: 200,
            config: decompressConfig,
            value: '?'
          });
        }
        return callback({
          code: 200,
          device: deviceId,
          config: result.config
        });
      }
      return callback({
        code: 404,
        config: '?',
        value: '?'
      });
    })();
  }

  setConfig(deviceId, transactions, callback) {
    (async () => {
      console.log(
        `Incoming Changeset:\n ${util.inspect(transactions, {
          showHidden: false,
          depth: null,
          colors: true
        })}`
      );
      console.log(`Remediating config for: ${deviceId}`);
      const device = await MOCK_DEVICES.findOne({
        deleted: false,
        name: deviceId
      });
      console.log(`Getting parser for OS: ${device.ostype}`);
      const deviceParser = await getDeviceParser(device.ostype);
      if (!deviceParser) {
        const e = `Unable to find a parser for ${deviceId} with type ${device.ostype}`;
        return callback(null, new Error(e));
      }
      const driver = getDriver(device.ostype, deviceParser);
      if (!driver) {
        const e = `Unable to find a driver for type: ${device.ostype}`;
        return callback(null, new Error(e));
      }
      const parsedConfig = driver.parse(device.config);
      const changes = [];
      parsedConfig.lines.forEach((element) => {
        lineToChangeSet(element, [], changes);
      });

      translateChangesToNativeConfig(
        changes.concat(transactions),
        deviceParser,
        async (native) => {
          console.log(
            `Remediation changeset successfully integrated into: ${deviceId}`
          );
          const newConfig = negation(bangOn(findEnd(native)));
          device.config = newConfig;
          const r = await MOCK_DEVICES.updateOne(
            { name: deviceId },
            { $set: device }
          );
          console.log(`Configuration saved for: ${deviceId}`);
          return callback({
            status: 200,
            results: {
              attemptedConfig: 'TODO'
            }
          });
        }
      );
    })();
  }

  restoreConfig(deviceId, config, callback) {
    (async () => {
      const result = await MOCK_DEVICES.findOne({
        deleted: false,
        name: deviceId
      });
      if (!result) {
        return callback(
          null,
          `Cannot call restoreConfig on ${deviceId} not found`
        );
      }
      result.config = config;
      const r = await MOCK_DEVICES.updateOne(
        { name: deviceId },
        { $set: { config } }
      );
      return callback('success');
    })();
  }

  getDeviceGroups(callback) {
    const groups = [];
    groups.push({ name: 'mock', devices: [] });
    return callback(groups);
  }

  /**
   * @param {Array} devices Array of unique id strings
   * @param {string} command The command to run against the devices
   * @param {object} options (optional) Possible specific options
   * @param {Function} callback The callback function
   */
  runCommand(devices, command, options = {}, callback) {
    (async () => {
      const cmd = command.split(' ')[0];
      const results = [];
      for (const deviceId of devices) {
        const device = await MOCK_DEVICES.findOne({
          deleted: false,
          name: deviceId
        });
        if (device) {
          let result = 'ok';
          if (Object.hasOwnProperty.call(device.actions, cmd)) {
            const r = await this.doAction(device.actions[cmd], device);
            results.push({ device: device.name, result: r, status: 'success' });
          } else {
            if (
              Object.hasOwnProperty.call(device.states, 'type')
              && device.states.type === 'Buffer'
            ) {
              const decompressedState = await decompressData(device.states);
              device.states = JSON.parse(decompressedState);
            }
            if (Object.hasOwnProperty.call(device.states[device.current_state], command)) {
              result = device.states[device.current_state][command];
              results.push({
                device: device.name,
                result,
                status: 'success'
              });
            } else {
              results.push({
                device: device.name,
                result,
                status: 'error'
              });
            }
          }
        } else {
          console.error(`${device.name} not found?`);
          results.push({
            device: device.name,
            result: 'no such device',
            status: 'error'
          });
        }
      }
      return callback(results);
    })();
  }

  async doAction(cmd, device) {
    switch (cmd.action) {
      case 'next_state':
        const max_state = device.states.length - 1;
        if (device.current_state < max_state) {
          device.current_state += 1;
        } else {
          device.current_state = 0;
        }
        break;
      case 'reset_state':
        device.current_state = 0;
        break;
      case 'turn_on':
        device.online = true;
        break;
      case 'turn_off':
        device.online = false;
        break;
      case 'overwrite_config':
        device.config = device.states[device.current_state][cmd.value];
        break;
      default:
        console.error(`unsupported action ${action}`);
    }
    const r = await MOCK_DEVICES.updateOne(
      { name: device.name },
      { $set: device }
    );
    return 'Device state has been updated!';
  }
}

util.inherits(MockDevice, EventEmitterCl);
module.exports = MockDevice;
