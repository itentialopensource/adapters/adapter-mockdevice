## 2.0.1 [08-11-2023]

* Add response code to getConfig

See merge request itentialopensource/adapters/adapter-mockdevice!13

---

## 2.0.0 [06-01-2023]

* Major/adapt 2666

See merge request itentialopensource/adapters/staging/adapter-mockdevice!12

---