# Adapter for Mockdevice


## Overview

This adapter is a mock implementation of device broker. It should be used if there is a need of mock devices for testing purposes.

## Getting Started

These instructions will help you get a copy of the project on your local machine for development and testing. Reading this section is also helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Prerequisites

The following is a list of required packages for installation on the system the adapter will run on:

```text
Node.js
npm
Git
```

### How to Install

1. Set up the name space location in your IAP node_modules.

```bash
cd /opt/pronghorn/current/node_modules (* could be in a different place)
if the @itentialopensource directory does not exist, create it:
    mkdir @itentialopensource
```

2. Clone/unzip/tar the adapter into your IAP environment.

```bash
cd \@itentialopensource
git clone git@gitlab.com:\@itentialopensource/adapters/adapter-mockdevice
or
unzip adapter-mockdevice.zip
or
tar -xvf adapter-mockdevice.tar
```

3. Run the adapter install script.

```bash
cd adapter-mockdevice
npm run adapter:install
```

4. Restart IAP

```bash
systemctl restart pronghorn
```

5. Change the adapter service instance configuration (host, port, credentials, etc) in IAP Admin Essentials GUI

npm run adapter:install can be dependent on where the adapter is installed and on the version of IAP so it is subject to fail. If this happens you can replace step 3-5 above with these:

3. Install adapter dependencies and check the adapter.

```bash
cd adapter-mockdevice
npm run install
npm run lint:errors
npm run test
```

4. Restart IAP

```bash
systemctl restart pronghorn
```

5. Create an adapter service instance configuration in IAP Admin Essentials GUI

### Adapter Properties and Descriptions

This section provides sample properties that can be used to configure the adpater in IAP.

```JSON
{
  "name": "mockDevice",
  "model": "@itential/adapter-mockdevice",
  "type": "Adapter",
  "properties": {
    "id": "mockDevice",
    "type": "MockDevice",
    "brokers": [
      "device"
    ],
    "groups": [],
    "properties": {}
  },
  "isEncrypted": true,
  "loggerProps": {
    "description": "Logging",
    "log_max_files": 10,
    "log_max_file_size": 10485760,
    "log_level": "warn",
    "log_directory": "/opt/itential/logs",
    "log_filename": "mockDevice.log",
    "log_timezone_offset": 0,
    "console_level": "warn",
    "syslog": {
      "level": "warning",
      "host": "127.0.0.1",
      "port": 514,
      "protocol": "udp4",
      "facility": "local0",
      "type": "BSD",
      "path": "",
      "pid": "process.pid",
      "localhost": "",
      "app_name": "",
      "eol": ""
    }
  },
  "virtual": false
}
```


## Configuring MongoDB

Inside of your pronghorn database, a new collection will need to be created called `mock_devices`. Inside of this collection, all the mock adapters that are to be used will live.
Two example devices are listed below:

```JSON
{
    "_id" : ObjectId("643ebeb47a16b404b2cd9ffe"),
    "name" : "nx03-atl",
    "authgroup" : "admin",
    "ipaddress" : "172.20.100.226",
    "address" : "172.20.100.226",
    "port" : 22.0,
    "device-type" : "cli",
    "ostype" : "cisco-nx-cli-5.22",
    "ned" : "cisco-nx-cli-5.22",
    "admin" : true,
    "tenant" : "",
    "chosenAdapter" : "mockAdapter",
    "origin" : "mockAdapter",
    "host" : "mockAdapter",
    "online" : "true",
    "deleted" : false,
    "actions" : [
        "addDevice",
        "deleteDevice",
        "getConfig",
        "getDevice",
        "getDevicesFiltered",
        "isAlive",
        "loadConfig",
        "restoreConfig",
        "runCommand",
        "setConfig"
    ]
}
```

```JSON
{
    "_id" : ObjectId("643ebed97a16b404b2cd9fff"),
    "name" : "Cisco-IOS-Cypress",
    "authgroup" : "admin",
    "ipaddress" : "172.20.100.226",
    "address" : "172.20.100.226",
    "port" : 22.0,
    "device-type" : "cli",
    "ostype" : "cisco-nx-cli-5.22",
    "ned" : "cisco-nx-cli-5.22",
    "admin" : true,
    "tenant" : "",
    "chosenAdapter" : "mockAdapter",
    "origin" : "mockAdapter",
    "host" : "mockAdapter",
    "online" : "true",
    "deleted" : false,
    "actions" : [
        "addDevice",
        "deleteDevice",
        "getConfig",
        "getDevice",
        "getDevicesFiltered",
        "isAlive",
        "loadConfig",
        "restoreConfig",
        "runCommand",
        "setConfig"
    ]
}
```