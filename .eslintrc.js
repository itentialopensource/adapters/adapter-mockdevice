module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true
  },
  extends: 'airbnb-base',
  plugins: [
    'json'
  ],
  parserOptions: {
    sourceType: 'module'
  },
  rules: {
    'max-len': 'warn',
    'comma-dangle': ['error', 'never'],
    'no-unused-vars': 'warn',
    'import/no-dynamic-require': 'warn',
    'no-param-reassign': 'warn',
    'no-undef': 'warn',
    camelcase: 'warn',
    'no-case-declarations': 'warn',
    'class-methods-use-this': 'off',
    'no-await-in-loop': 'warn',
    'no-restricted-syntax': 'warn',
    'consistent-return': 'warn',
    'global-require': 'warn',
    'no-underscore-dangle': 'warn'
  }
};
