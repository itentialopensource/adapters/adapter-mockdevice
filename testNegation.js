const data = 'You are empty handed\nno You are empty handed\nYou are carrying:\n  A leaflet\n  A sword\n  no A sword\n  A shield\n!';
console.log(data);

/**
 * @param native
 * @param line
 */
function removeLine(native, line) {
  const lines = native.split('\n');
  const index = lines.indexOf(line);
  if (index > -1) {
    lines.splice(index, 1);
  }
  return lines.join('\n');
}

/**
 * @param native
 */
function negation(native) {
  const regex = /no .*/g;
  const lines = native.split('\n');
  console.log(`${lines.length} lines of config`);
  const negatives = lines.filter((line) => line.match(regex));

  let updated = native;
  negatives.forEach((n) => {
    const pieces = n.split('no ');
    let ltr = pieces[1];
    if (pieces[0].length > 0) {
      ltr = pieces[0].split('no ') + ltr;
    }
    updated = removeLine(updated, ltr);
    updated = removeLine(updated, n);
  });
  return updated;
}

const d = negation(data);
console.log(d);
