
## 2.1.3 [10-15-2024]

* Changes made at 2024.10.14_20:23PM

See merge request itentialopensource/adapters/adapter-mockdevice!20

---

## 2.1.2 [09-20-2024]

* update dependencies

See merge request itentialopensource/adapters/adapter-mockdevice!18

---

## 2.1.1 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/adapter-mockdevice!17

---

## 2.1.0 [01-08-2024]

* 2023 migration changes

See merge request itentialopensource/adapters/adapter-mockdevice!15

---

## 2.0.1 [08-11-2023]

* Add response code to getConfig

See merge request itentialopensource/adapters/adapter-mockdevice!13

---

## 2.0.0 [06-01-2023]

* Major/adapt 2666

See merge request itentialopensource/adapters/staging/adapter-mockdevice!12

---

## 1.0.0 [05-30-2023]

* Major/adapt 2666

See merge request itentialopensource/adapters/staging/adapter-mockdevice!12

---

## 0.0.2 [10-18-2021]

* jira_ticket_property_get failed. jq returned a non-zero exit code.

See ticket [LBCD-466](https://itential.atlassian.net/rest/api/3/browse/LBCD-466).

---
