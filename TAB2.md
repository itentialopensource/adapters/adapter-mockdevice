
## Table of Contents 

  - [Specific Adapter Information](#specific-adapter-information) 
    - [Authentication](#authentication) 
    - [Sample Properties](#sample-properties) 
    - [Swagger](#swagger) 
  - [Generic Adapter Information](#generic-adapter-information) 

## Specific Adapter Information
### Authentication

This document will go through the steps for authenticating the Mock Device adapter with. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

#### No Authentication
The Winston Syslog Adapter does not require any authentication.

#### Troubleshooting
- Make sure you copied over the correct username and password.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Investigate the logs
### Sample Properties

Sample Properties can be used to help you configure the adapter in the Itential Automation Platform. You will need to update connectivity information such as the host, port, protocol and credentials.

```json
  "properties": {
    "id": "mockdevice",
    "type": "MockDevice",
    "properties": {
      "devices": [
        {
          "prefix": "ATL",
          "type": "cisco-ios-xr",
          "count": "2"
        },
        {
          "prefix": "NYC",
          "type": "f5-bigip",
          "count": "7"
        }
      ]
    },
    "brokers": [
      "device"
    ],
    "groups": []
  }
```
### Swagger

Note: The content for this section may be missing as its corresponding .json file is unavailable. This sections will be updated once adapter-openapi.json file is added.
## [Generic Adapter Information](https://gitlab.com/itentialopensource/adapters/adapter-mockdevice/-/blob/master/README.md) 

