/** Local pipeline configuration.
 * @module
 */

/** Logger.
 * @name log
 * @global
 */
if (!global.log) global.log = console;

/** pre-commit tasks.
 * @description Tasks that run before a commit is accepted. Tasks will run in
 * the order that they are listed.
 */
const preCommitTasks = [
  //'npm run lint:fix:staged',
  //'npm run format:staged',
  'npm run lint',
  'npm run quality:spellcheck',
];

/** pre-push tasks.
 * @description Tasks that run before a push is accepted. Tasks will run in the
 * order that they are listed.
 */
const prePushTasks = ['npm run test'];

/** husky git hook configuration. */
const config = {
  hooks: {
    'pre-commit': preCommitTasks.join(' && '),
    'pre-push': prePushTasks.join(' && '),
  },
};

if (typeof require !== 'undefined' && require.main === module) {
  // When a script.
  log.debug(JSON.stringify(config, null, 2));
} else {
  // When a module.
  module.exports = config;
}
