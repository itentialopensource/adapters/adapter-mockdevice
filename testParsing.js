const fs = require('fs');
const path = require('path');
const EventEmitterCl = require('events').EventEmitter;
const util = require('util');

const localPath = '/Users/michaelwymer/Projects/IAP/repos/app-configuration_manager';
const { getDriver } = require(`${localPath}/shared/device-config/index.js`);
const {
  getParserForDevice
} = require(`${localPath}/shared/device-config/getTestDeviceParser.js`);
const {
  translateChangesToNativeConfig
} = require(`${localPath}/config-tools/nativeConfigConverter.js`);

const type = 'cisco-ios-xr';
const config = fs.readFileSync(
  path.join(__dirname, `./configs/${type}`),
  'utf8'
);

const deviceParser = getParserForDevice(type);
console.log('got parser');
const driver = getDriver(type, deviceParser);
console.log('got driver');
const parsedConfig = driver.parse(config);
// const parsedConfig = convertConfigToJson(device.config,device.ostype,deviceParser);
console.log('ran parser');
console.log(util.inspect(parsedConfig, false, null, true /* enable colors */));

const after = {
  deviceType: 'cisco-ios',
  lines: [
    {
      id: '611e973226453af0',
      words: ['feature', 'privilege'],
      lines: [],
      evalMode: 'required',
      fixMode: 'manual',
      severity: 'warning'
    },
    {
      id: '611e973220ea350d',
      words: ['interface', 'xyz'],
      lines: [
        {
          id: '611e9732b97c4394',
          words: ['description', '"Description for xyz"'],
          lines: [],
          evalMode: 'required',
          fixMode: 'manual',
          severity: 'warning'
        },
        {
          id: '611e9732a2fac75d',
          words: ['no', 'ip', 'address'],
          lines: [],
          evalMode: 'required',
          fixMode: 'manual',
          severity: 'warning'
        }
      ],
      evalMode: 'required',
      fixMode: 'manual',
      severity: 'warning'
    },
    {
      id: '611e9732bcc8d7ab',
      words: ['interface', 'wwz'],
      lines: [
        {
          id: '611e9732d79d943f',
          words: ['description', '"Description for wwz"'],
          lines: [],
          evalMode: 'required',
          fixMode: 'manual',
          severity: 'warning'
        }
      ],
      evalMode: 'required',
      fixMode: 'manual',
      severity: 'warning'
    }
  ]
};

const changeset = [
  {
    parents: ['interface xyz'],
    new: 'description "Description for xyz"',
    old: null
  },
  {
    parents: ['interface xyz'],
    new: 'no ip address',
    old: null
  },
  {
    parents: ['interface wwz'],
    new: 'description "Description for wwz"',
    old: null
  },
  {
    parents: [],
    new: 'feature privilege',
    old: null
  }
];

/**
 * @param lineObj
 * @param p
 * @param changes
 */
function lineToChangeSet(lineObj, p, changes) {
  const line = lineObj.words.join(' ');
  const parent = [...p];
  if (lineObj.lines.length === 0) {
    changes.push({
      parents: p,
      new: line,
      old: null
    });
  } else {
    parent.push(line);
    lineObj.lines.forEach((element) => {
      lineToChangeSet(element, parent, changes);
    });
  }
}

/**
 * @param native
 */
function bangOn(native) {
  let indent = 0;
  const newLines = [];
  const lines = native.split('\n');
  lines.forEach((line) => {
    const spacecount = line.search(/\S|$/);
    console.log(spacecount);
    if (spacecount < indent) {
      let bString = '';
      console.log('bangon!');
      for (let index = 0; index < spacecount; index += 1) {
        bString += ' ';
      }
      bString += '!';
      console.log(`add ${bString}`);
      newLines.push(bString);
    }
    newLines.push(line);
    indent = spacecount;
  });
  return newLines.join('\n');
}

/**
 * @param native
 */
function findEnd(native) {
  const lines = native.split('\n');
  const index = lines.indexOf('end');
  if (index > -1) {
    lines.splice(index, 1);
    lines.push('end');
  }
  return lines.join('\n');
}

const changes = [];

parsedConfig.lines.forEach((element) => {
  lineToChangeSet(element, [], changes);
});

// console.log(util.inspect(changes, false, null, true /* enable colors */));

translateChangesToNativeConfig(changes, deviceParser, (native) => {
  const fixed = bangOn(findEnd(native));
  console.log(fixed);
});
