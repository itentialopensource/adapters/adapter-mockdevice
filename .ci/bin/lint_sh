#!/usr/bin/env bash

#
# Lint shell scripts.
#
lint_sh() {
  local file_paths="$1"
  local fail=0

  if [ -z "$file_paths" ]; then
    echo "The file paths argument was not given."
    return 1
  fi

  #  Must be preset, e.g., npm i --save-dev shellcheck
  command -v shellcheck > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "shellcheck command not found."
    return 1
  fi

  echo "Begin shell script file linting."
  for file_path in $file_paths; do
    if [ -e "$file_path" ]; then

      # On path? Use it.
      command -v shellcheck > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        shellcheck \
          --check-sourced    \
          --color=auto       \
          --external-sources \
          --severity="style"  \
          --exclude SC1090   \
          --exclude SC1091   \
          --exclude SC2181   \
          --exclude SC2153   \
          "$file_path"

          if [ $? -ne 0 ]; then
            echo "Fail. $file_path."
            fail=1
          else
            echo "Pass. $file_path."
          fi

      # npm package wrapper?
      else
        command -v ./node_modules/.bin/shellcheck > /dev/null 2>&1
        if [ $? -eq 0 ]; then
          ./node_modules/.bin/shellcheck \
          --check-sourced    \
          --color=auto       \
          --external-sources \
          --severity="style"  \
          --exclude SC1090   \
          --exclude SC1091   \
          --exclude SC2181   \
          --exclude SC2153   \
          "$file_path"

          if [ $? -ne 0 ]; then
            echo "Fail. $file_path."
            fail=1
          else
            echo "Pass. $file_path."
          fi
        else
          echo 'No shellcheck command found. Exiting.'
          exit 1
        fi
      fi
    else
      echo "Fail. Cannot find $file_path."
      fail=1
    fi
  done
  echo "End shell script file linting."

  if [ "$fail" != "0" ]; then
    return 1
  fi
}

main() {
  echo "Begin linting."

  # Shell script lint.
    # Ideal but not portable to Busybox grep.
    # file_paths=$(grep -rl '^#!/.*sh' . \
    # --exclude-dir=".git"               \
    # --exclude-dir="**/node_modules"     \
    # )

    # Portable to Busybox.
    file_paths=$(find . -type f \
    -not -path "./node_modules/*" \
    -not -path "./.git/*" \
    -not -path "./.ci/report/*"  \
    -not -path "./.ci/tmp/*"  \
    -exec grep -rl '^#!/.*sh' {} \;)

    if [ -z "$file_paths" ]; then
      echo "Shell script linting is not necessary. No shell scripts detected."
      exit 0
    else
      lint_sh "$file_paths"
      [ $? -eq 0 ] || exit 1
    fi

  echo "End linting."
}

main
