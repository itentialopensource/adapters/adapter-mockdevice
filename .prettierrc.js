#!/usr/bin/env node

/** Formatter configuration.
 * @module
 */

/** Logger.
 * @name log
 * @global
 */
if (!global.log) global.log = console;

/** prettier configuration. */
const config = {
  arrowParens: 'avoid',
  bracketSpacing: true,
  endOfLine: 'lf',
  jsxSingleQuote: true,
  jsxBracketSameLine: true,
  printWidth: 80,
  proseWrap: 'never',
  quoteProps: 'as-needed',
  semi: true,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'all',
  useTabs: false,
};

if (typeof require !== 'undefined' && require.main === module) {
  // When a script.
  log.debug(JSON.stringify(config, null, 2));
} else {
  // When a module.
  module.exports = config;
}
