/** Documentation configuration.
 * @module
 */

/** Logger.
 * @name log
 * @global
 */
if (!global.log) global.log = console;

/** JSDoc configuration. */
const config = {
  sourceType: 'module',
  source: {
    include: ['README.md'],
  },
  tags: {
    allowUnknownTags: false,
    dictionaries: ['jsdoc'],
  },
  plugins: ['plugins/markdown', 'plugins/summarize'],
  templates: {
    cleverLinks: true,
    monospaceLinks: true,
    useLongnammeInNav: false,
    showInheritedInNav: true,
  },
  opts: {
    encoding: 'utf8',
    recurse: true,
    destination: 'docs/api',
    tutorials: 'docs/api/tutorials',
    verbose: true,
  },
};

if (typeof require !== 'undefined' && require.main === module) {
  // Run as a script.
  log.debug(JSON.stringify(config, null, 2));
} else {
  // Required module.
  module.exports = config;
}
